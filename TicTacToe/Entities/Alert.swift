//
//  Alert.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 28.05.2021.
//

import UIKit

struct Alert {

    enum AlertActions {
        case left,
             right
    }

    let title: String
    let message: String

    let leftButtonTitle: String
    let rightButtonTitle: String

    let completion: ((AlertActions) -> Void)?

    var messageFont: UIFont? = nil
    var messageColor: UIColor? = nil

}
