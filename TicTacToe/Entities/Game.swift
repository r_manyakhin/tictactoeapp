//
//  Game.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 28.05.2021.
//

import Foundation

let gameLevel: Int = 3

struct Game {

    var boardValues: [[String]]
    var userMove: Bool = true

}
