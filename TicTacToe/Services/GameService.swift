//
//  GameService.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 28.05.2021.
//

import Foundation

protocol GameServiceProtocol {

    var game: Game { get }

    func userGameMove(section: Int, index: Int, completion: (_ goodMove: Bool) -> Void)

    func checkNextMove(completion: @escaping (_ winning: String?) -> Void)

    func makeAiMove(completion: @escaping (_ winning: String?) -> Void)

    func restartGame()

}

class GameService: GameServiceProtocol {

    // MARK: - Public properties

    private(set) var game: Game

    // MARK: - Private properties

    // MARK: - Lifecycle

    init(with game: Game) {
        self.game = game
    }

    // MARK: - Public methods

    func userGameMove(section: Int, index: Int, completion: (_ goodMove: Bool) -> Void) {
        let goodMove = game.boardValues[section][index].isEmpty

        if goodMove {
            game.boardValues[section][index] = "x"

            game.userMove = false
        }

        completion(goodMove)
    }

    func checkNextMove(completion: @escaping (_ winning: String?) -> Void) {
        let board = game.boardValues.reduce([], +)

        if winning(board: board, player: "x") {
            completion("x")
        } else if winning(board: board, player: "o") {
            completion("o")
        } else if emptyIndexies(board: board).isEmpty {
            completion("")
        } else {
            completion(nil)
        }
    }

    func makeAiMove(completion: @escaping (_ winning: String?) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) { [weak self] in
            guard let self = self else { return }

            var board = self.game.boardValues.reduce([], +)

            let bestSpot = self.minimax(board: board, player: "o")

            var index = bestSpot.index ?? 0
            let section = Int(index / 3)
            index -= section * 3

            self.game.boardValues[section][index] = "o"
            self.game.userMove = true

            board = self.game.boardValues.reduce([], +)

            var winning: String? = nil
            if self.winning(board: board, player: "x") {
                winning = "x"
            } else if self.winning(board: board, player: "o") {
                winning = "o"
            } else if self.emptyIndexies(board: board).isEmpty {
                completion("")
            }

            completion(winning)
        }
    }

    func restartGame() {
        game = Game(boardValues: Array(repeating: Array(repeating: "", count: gameLevel), count: gameLevel))
    }

    // MARK: - Private methods

    private func minimax(board: [String], player: String) -> (score: Int, index: Int?) {
        let availSpots = emptyIndexies(board: board)

        if winning(board: board, player: "x") {
            return (score: -10, index: nil)
        } else if winning(board: board, player: "o") {
            return (score: 10, index: nil)
        } else if availSpots.isEmpty {
            return (score: 0, index: nil)
        }

        var moves: [(score: Int, index: Int?)] = []

        for i in availSpots.indices {
            var move = (score: 0, index: availSpots[i])

            var newBoard = board
            newBoard[availSpots[i]] = player

            if player == "o" {
                let result = minimax(board: newBoard, player: "x")
                move.score = result.score
            } else {
                let result = minimax(board: newBoard, player: "o")
                move.score = result.score
            }

            moves.append(move)
        }

        var bestMove = 0

        if player == "o" {
            var bestScore = -10000
            for i in moves.indices {
                if moves[i].score > bestScore {
                    bestScore = moves[i].score
                    bestMove = i
                }
            }
        } else {
            var bestScore = 10000
            for i in moves.indices {
                if moves[i].score < bestScore {
                    bestScore = moves[i].score
                    bestMove = i
                }
            }
        }

        return moves[bestMove]
    }

    private func emptyIndexies(board: [String]) -> [Int] {
        return board.enumerated().compactMap({ ($0.element == "o" || $0.element == "x") ? nil : $0.offset })
    }

    private func winning(board: [String], player: String) -> Bool {
        return (board[0] == player && board[1] == player && board[2] == player) ||
            (board[3] == player && board[4] == player && board[5] == player) ||
            (board[6] == player && board[7] == player && board[8] == player) ||
            (board[0] == player && board[3] == player && board[6] == player) ||
            (board[1] == player && board[4] == player && board[7] == player) ||
            (board[2] == player && board[5] == player && board[8] == player) ||
            (board[0] == player && board[4] == player && board[8] == player) ||
            (board[2] == player && board[4] == player && board[6] == player)
    }

}
