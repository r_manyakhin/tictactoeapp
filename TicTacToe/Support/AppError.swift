//
//  AppError.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 26.05.2021.
//

import Foundation

enum AppErrorCodes: Int {

    case SerrializationError = 9000,
         BadInternetError

}

struct AppError: Swift.Error, CustomStringConvertible {

    static var pleaseTryAgain: AppError {
        return AppError(
            description: "Упс, что-то пошло не так... Пожалуйста, попробуйте еще раз.",
            code: .SerrializationError)
    }

    static var badInternet: AppError {
        return AppError(
            description: "Упс, что-то пошло не так... Пожалуйста, проверьте ваше интернет подключение и попробуйте еще раз.",
            code: .BadInternetError)
    }

    var description: String
    var localizedDescription: String {
        return description
    }
    var code: Int

    init(description: String = "Unknown", code: Int = Int.max) {
        self.description = description
        self.code = code
    }

    init?(description: String?, code: Int?) {
        guard let description = description, let code = code else {
            return nil
        }
        self.description = description
        self.code = code
    }

    init(description: String = "Unknown", code: AppErrorCodes) {
        self.description = description
        self.code = code.rawValue
    }

}
