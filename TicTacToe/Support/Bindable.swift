//
//  Bindable.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 26.05.2021.
//

import Foundation

class Bindable<T> {

    typealias Listener = (T) -> ()

    private var listener: Listener?

    var value: T {
        didSet {
            listener?(value)
        }
    }

    init(value: T) {
        self.value = value
    }

    func unbind() {
        listener = nil
    }

    func bind(listener: @escaping Listener) {
        self.listener = listener
        self.listener?(value)
    }

}
