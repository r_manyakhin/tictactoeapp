//
//  Bundle+Extension.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 26.05.2021.
//

import Foundation

extension Bundle {

    var appName: String {
        return infoDictionary?["CFBundleName"] as? String ?? ""
    }

    var version: String {
        return infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    }

    var build: String {
        return infoDictionary?["CFBundleVersion"] as? String ?? ""
    }

}
