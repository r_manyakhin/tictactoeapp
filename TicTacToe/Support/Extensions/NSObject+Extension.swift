//
//  NSObject+Extension.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 26.05.2021.
//

import Foundation

extension NSObject {

    static var className: String {
        return String(describing: self)
    }

}
