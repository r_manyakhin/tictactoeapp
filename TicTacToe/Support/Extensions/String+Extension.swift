//
//  String+Extension.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 26.05.2021.
//

import Foundation

extension String {

    var localize: String {
        return self.localize()
    }

    func localize(comment: String = "", _ args: CVarArg...) -> String {
        let format = NSLocalizedString(self, comment: comment)
        return String(format: format, locale: Locale.current, arguments: args)
    }

}
