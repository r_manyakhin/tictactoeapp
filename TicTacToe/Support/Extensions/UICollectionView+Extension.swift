//
//  UICollectionView+Extension.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 26.05.2021.
//

import UIKit

extension UICollectionView {

    func registerCell<T: UICollectionViewCell>(
        _ type: T.Type = T.self,
        withIdentifier reuseIdentifier: String = String(describing: T.self)
    ) {
        register(T.self, forCellWithReuseIdentifier: reuseIdentifier)
    }

    func registerCellFromNib<T: UICollectionViewCell>(
        _ type: T.Type = T.self,
        withIdentifier reuseIdentifier: String = String(describing: T.self)
    ) {
        register(UINib(nibName: reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
    }

    func dequeueCell<T: UICollectionViewCell>(
        _ type: T.Type = T.self,
        withIdentifier reuseIdentifier: String = String(describing: T.self),
        for indexPath: IndexPath
    ) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? T else {
            fatalError("Unknown cell type (\(T.self)) for reuse identifier: \(reuseIdentifier)")
        }
        return cell
    }

}
