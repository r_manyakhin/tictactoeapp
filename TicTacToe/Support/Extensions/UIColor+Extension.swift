//
//  UIColor+Extension.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 26.05.2021.
//

import UIKit

extension UIColor {

    convenience init(hex: Int, alpha: CGFloat = 1.0) {
        self.init(
            red: CGFloat((hex >> 16) & 0xFF) / 255,
            green: CGFloat((hex >> 8) & 0xFF) / 255,
            blue: CGFloat(hex & 0xFF) / 255,
            alpha: alpha
        )
    }

}

extension UIColor {

    static var purple: UIColor {
        return UIColor(hex: 0x7856D9)
    }

    static var red: UIColor {
        return UIColor(hex: 0xFF0000)
    }

}
