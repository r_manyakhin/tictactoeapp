//
//  UIFont+Extension.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 28.05.2021.
//

import UIKit

extension UIFont {

    static func ownHand(size: CGFloat) -> UIFont? {
        return UIFont(name: "OwnHand", size: size)
    }

}
