//
//  UITableView+Extension.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 26.05.2021.
//

import UIKit

extension UITableView {

    func registerCell<T: UITableViewCell>(
        _ type: T.Type = T.self,
        withIdentifier reuseIdentifier: String = String(describing: T.self)
    ) {
        register(T.self, forCellReuseIdentifier: reuseIdentifier)
    }

    func registerCellFromNib<T: UITableViewCell>(
        _ type: T.Type = T.self,
        withIdentifier reuseIdentifier: String = String(describing: T.self)
    ) {
        register(UINib(nibName: reuseIdentifier, bundle: nil), forCellReuseIdentifier: reuseIdentifier)
    }

    func dequeueCell<T: UITableViewCell>(
        _ type: T.Type = T.self,
        withIdentifier reuseIdentifier: String = String(describing: T.self)
    ) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: reuseIdentifier) as? T else {
            fatalError("Unknown cell type (\(T.self)) for reuse identifier: \(reuseIdentifier)")
        }
        return cell
    }

}
