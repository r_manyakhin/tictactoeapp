//
//  UIView+Extension.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 26.05.2021.
//

import UIKit

extension UIView {

    @objc func loadFromNib(named: String? = nil) -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: named ?? String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }

}
