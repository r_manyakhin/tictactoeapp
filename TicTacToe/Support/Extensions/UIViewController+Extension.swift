//
//  UIViewController+Extension.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 26.05.2021.
//

import UIKit

extension UIViewController {

    // Nib
    static func nibInit() -> Self {
        return self.init(nibName: className, bundle: nil)
    }

    // Storyboard
    static func controller(
        from storyboard: Storyboard,
        with identifier: String? = nil
    ) -> Self {
        return controllerInStoryboard(UIStoryboard(name: storyboard.name, bundle: nil), identifier: identifier ?? className)
    }

    private static func instantiateControllerInStoryboard<T: UIViewController>(
        _ storyboard: UIStoryboard,
        identifier: String
    ) -> T {
        return storyboard.instantiateViewController(withIdentifier: identifier) as! T
    }

    private static func controllerInStoryboard(
        _ storyboard: UIStoryboard,
        identifier: String
    ) -> Self {
        return instantiateControllerInStoryboard(storyboard, identifier: identifier)
    }

    private static func controllerInStoryboard(_ storyboard: UIStoryboard) -> Self {
        return controllerInStoryboard(storyboard, identifier: className)
    }

}
