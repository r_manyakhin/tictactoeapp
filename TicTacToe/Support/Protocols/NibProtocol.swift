//
//  NibProtocol.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 26.05.2021.
//

import UIKit

protocol NibInitializable {

    var view: UIView! {get set}

}

extension NibInitializable where Self: BaseView {

    func nibSetup() {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
    }

}

protocol NibProtocol {

    static func nib() -> UINib

}

extension NibProtocol where Self: NSObject {

    static func nib() -> UINib {
        return UINib(nibName: className, bundle: nil)
    }

}

extension UITableViewCell: NibProtocol {}

extension UICollectionViewCell: NibProtocol {}
