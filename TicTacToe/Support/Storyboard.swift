//
//  Storyboard.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 26.05.2021.
//

import Foundation

struct Storyboard {

    let name: String

    static let main = Storyboard(name: "Main")

    static let game = Storyboard(name: "Game")

    static let alert = Storyboard(name: "Alert")

}
