//
//  AlertPresenter.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 28.05.2021.
//

import Foundation

protocol AlertPresenterProtocol {

    func moduleWasLoaded()

    func leftButtonClicked()

    func rightButtonClicked()

}

class AlertPresenter: AlertPresenterProtocol {

    // MARK: - Public properties

    // MARK: - Private properties

    private let alert: Alert

    private weak var view: AlertViewProtocol?
    private var wireframe: AlertWireframeProtocol?

    // MARK: - Lifecycle

    init(with alert: Alert, view: AlertViewProtocol, wireframe: AlertWireframeProtocol?) {
        self.alert = alert
        self.view = view
        self.wireframe = wireframe
    }

    // MARK: - Public methods

    func moduleWasLoaded() {
        view?.updateTitle(alert.title)
        view?.updateMessage(alert.message)
        view?.updateLeftButtonTitle(alert.leftButtonTitle)
        view?.updateRightButtonTitle(alert.rightButtonTitle)
        if let font = alert.messageFont {
            view?.updateMessageFont(font)
        }
        if let color = alert.messageColor {
            view?.updateMessageColor(color)
        }
    }

    func leftButtonClicked() {
        wireframe?.hideAlert(completion: { [weak self] in
            guard let self = self else { return }

            self.alert.completion?(.left)
        })
    }

    func rightButtonClicked() {
        wireframe?.hideAlert(completion: { [weak self] in
            guard let self = self else { return }

            self.alert.completion?(.right)
        })
    }

    // MARK: - Private methods
}
