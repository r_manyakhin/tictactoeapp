//
//  AlertViewController.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 28.05.2021.
//

import UIKit

protocol AlertViewProtocol: class {

    func updateTitle(_ title: String?)

    func updateMessage(_ message: String?)

    func updateLeftButtonTitle(_ title: String?)

    func updateRightButtonTitle(_ title: String?)

    func updateMessageFont(_ font: UIFont)

    func updateMessageColor(_ color: UIColor)

}

class AlertViewController: UIViewController {

    // MARK: - Outlets

    @IBOutlet private weak var backView: UIView!

    @IBOutlet private weak var titleLabel: UILabel!

    @IBOutlet private weak var messageLabel: UILabel!

    @IBOutlet private weak var leftButton: UIButton!

    @IBOutlet private weak var rightButton: UIButton!

    // MARK: - Public properties

    var presenter: AlertPresenterProtocol?

    // MARK: - Private properties

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        backView.layer.cornerRadius = 12.0
        backView.clipsToBounds = true

        leftButton.setTitleColor(.purple, for: .normal)
        leftButton.addTarget(self, action: #selector(leftButtonClicked(_:)), for: .touchUpInside)

        rightButton.setTitleColor(.red, for: .normal)
        rightButton.addTarget(self, action: #selector(rightButtonClicked(_:)), for: .touchUpInside)

        presenter?.moduleWasLoaded()
    }

    // MARK: - Public methods

    // MARK: - Private methods

    // MARK: - Outlet actions

    @IBAction private func leftButtonClicked(_ sender: UIButton) {
        presenter?.leftButtonClicked()
    }

    @IBAction private func rightButtonClicked(_ sender: UIButton) {
        presenter?.rightButtonClicked()
    }

}

// MARK: - AlertViewProtocol

extension AlertViewController: AlertViewProtocol {

    func updateTitle(_ title: String?) {
        titleLabel.text = title
    }

    func updateMessage(_ message: String?) {
        messageLabel.text = message
    }

    func updateLeftButtonTitle(_ title: String?) {
        leftButton.setTitle(title, for: .normal)
    }

    func updateRightButtonTitle(_ title: String?) {
        rightButton.setTitle(title, for: .normal)
    }

    func updateMessageFont(_ font: UIFont) {
        messageLabel.font = font
    }

    func updateMessageColor(_ color: UIColor) {
        messageLabel.textColor = color
    }

}
