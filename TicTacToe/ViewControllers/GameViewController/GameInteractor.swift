//
//  GameInteractor.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 28.05.2021.
//

import Foundation

protocol GameInteractorProtocol {

    func getGameStatus(completion: (_ boardValues: [[String]], _ userMove: Bool) -> Void)

    func userGameMove(section: Int, index: Int, completion: (_ goodMove: Bool) -> Void)

    func checkNextMove(completion: @escaping (_ winning: String?) -> Void)

    func makeAiMove(completion: @escaping (_ winning: String?) -> Void)

    func restartGame()

}

class GameInteractor: GameInteractorProtocol {

    // MARK: - Public properties

    // MARK: - Private properties

    private weak var presenter: GamePresenterProtocol?
    private var gameService: GameServiceProtocol

    // MARK: - Lifecycle

    init(presenter: GamePresenterProtocol, service: GameServiceProtocol) {
        self.presenter = presenter
        self.gameService = service
    }

    // MARK: - Public methods

    func getGameStatus(completion: (_ boardValues: [[String]], _ userMove: Bool) -> Void) {
        completion(gameService.game.boardValues, gameService.game.userMove)
    }

    func userGameMove(section: Int, index: Int, completion: (_ goodMove: Bool) -> Void) {
        gameService.userGameMove(section: section, index: index, completion: completion)
    }

    func checkNextMove(completion: @escaping (_ winning: String?) -> Void) {
        gameService.checkNextMove(completion: completion)
    }

    func makeAiMove(completion: @escaping (_ winning: String?) -> Void) {
        gameService.makeAiMove(completion: completion)
    }

    func restartGame() {
        gameService.restartGame()
    }

    // MARK: - Private methods

}
