//
//  GamePresenter.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 28.05.2021.
//

import UIKit

protocol GamePresenterProtocol: class {

    func moduleWasLoaded()

    func backButtonClicked()

    func userGameMove(section: Int, index: Int)

}

class GamePresenter: GamePresenterProtocol {

    // MARK: - Public properties

    var interator: GameInteractorProtocol?

    // MARK: - Private properties

    private weak var view: GameViewProtocol?
    private var wireframe: MainWireframeProtocol?

    // MARK: - Lifecycle

    init(view: GameViewProtocol, wireframe: MainWireframeProtocol?) {
        self.view = view
        self.wireframe = wireframe
    }

    // MARK: - Public methods

    func moduleWasLoaded() {
        interator?.getGameStatus(completion: { [weak self] boardValues, userMove in
            guard let self = self else { return }

            self.view?.updateBoard(boardValues, userMove: userMove)
        })
    }

    func backButtonClicked() {
        let alert = Alert(title: "Finish the game".localize, message: "Do you really want to quit?".localize, leftButtonTitle: "Yes".localize, rightButtonTitle: "No".localize) { [weak self] action in
            guard let self = self else { return }
            if action == .left {
                self.wireframe?.popController()
            }
        }

        wireframe?.showAlert(alert)
    }

    func userGameMove(section: Int, index: Int) {
        interator?.userGameMove(section: section, index: index, completion: { [weak self] goodMove in
            guard let self = self else { return }

            if goodMove {
                self.interator?.getGameStatus(completion: { [weak self] boardValues, userMove in
                    guard let self = self else { return }

                    self.view?.updateBoard(boardValues, userMove: userMove)
                })

                self.interator?.checkNextMove(completion: { [weak self] winning in
                    guard let self = self else { return }

                    if let winning = winning {
                        self.winning(winning)
                    } else {
                        self.view?.waitAiMove()

                        self.interator?.makeAiMove { [weak self] winning in
                            guard let self = self else { return }

                            self.interator?.getGameStatus(completion: { [weak self] boardValues, userMove in
                                guard let self = self else { return }

                                self.view?.updateBoard(boardValues, userMove: userMove)

                                if let winning = winning {
                                    self.winning(winning)
                                }
                            })
                        }
                    }
                })
            }
        })
    }

    // MARK: - Private methods

    private func winning(_ player: String) {
        var alert = Alert(
            title: (player.isEmpty ? "No winner :(" : "The winner is").localize,
            message: (player.isEmpty ? "" : player).localize,
            leftButtonTitle: "Finish".localize,
            rightButtonTitle: "Repeat".localize,
            completion: { [weak self] action in
                guard let self = self else { return }

                switch action {
                case .left:
                    self.wireframe?.popController()
                case .right:
                    self.restartGame()
                }
            }
        )
        if !player.isEmpty {
            alert.messageFont = UIFont.ownHand(size: 80.0)
            alert.messageColor = player == "x" ? .purple : .red
        }

        wireframe?.showAlert(alert)
    }

    private func restartGame() {
        interator?.restartGame()

        moduleWasLoaded()
    }

}
