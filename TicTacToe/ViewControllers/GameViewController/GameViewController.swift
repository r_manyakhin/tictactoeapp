//
//  GameViewController.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 28.05.2021.
//

import UIKit

protocol GameViewProtocol: class {

    func updateBoard(_ values: [[String]], userMove: Bool)

    func waitAiMove()

}

class GameViewController: UIViewController {

    // MARK: - Outlets

    @IBOutlet private weak var backButton: UIButton!

    @IBOutlet private weak var gameBoard: UICollectionView!

    @IBOutlet private weak var indicatorView: UIActivityIndicatorView!

    // MARK: - Public properties

    var presenter: GamePresenterProtocol?

    // MARK: - Private properties

    private var gameBoardValues: [[String]] = []
    private var userMove: Bool = false

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        backButton.setTitle("< Back".localize, for: .normal)
        backButton.setTitleColor(.purple, for: .normal)
        backButton.addTarget(self, action: #selector(backButtonClicked(_:)), for: .touchUpInside)

        gameBoard.registerCellFromNib(GameCollectionViewCell.self)
        gameBoard.delegate = self
        gameBoard.dataSource = self

        indicatorView.color = .purple
        indicatorView.stopAnimating()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        presenter?.moduleWasLoaded()
    }

    // MARK: - Public methods

    // MARK: - Private methods

    // MARK: - Outlet actions

    @IBAction private func backButtonClicked(_ sender: UIButton) {
        presenter?.backButtonClicked()
    }

}

// MARK: - GameViewProtocol

extension GameViewController: GameViewProtocol {

    func updateBoard(_ values: [[String]], userMove: Bool) {
        gameBoardValues = values
        self.userMove = userMove

        gameBoard.isUserInteractionEnabled = true
        indicatorView.stopAnimating()

        gameBoard.reloadData()
    }

    func waitAiMove() {
        gameBoard.isUserInteractionEnabled = false
        indicatorView.startAnimating()
    }

}

// MARK: - UICollectionViewDelegate

extension GameViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.userGameMove(section: indexPath.section, index: indexPath.row)
    }

}

// MARK: - UICollectionViewDataSource

extension GameViewController: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return gameBoardValues.count
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gameBoardValues[section].count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(GameCollectionViewCell.self, for: indexPath)

        let value = gameBoardValues[indexPath.section][indexPath.row]
        cell.updateUI(with: value, userMove: userMove)

        return cell
    }

}

// MARK: - UICollectionViewDelegateFlowLayout

extension GameViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAt section: Int
    ) -> CGFloat {
        return 0.0
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAt section: Int
    ) -> CGFloat {
        return 0.0
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        referenceSizeForFooterInSection section: Int
    ) -> CGSize {
        return .zero
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        referenceSizeForHeaderInSection section: Int
    ) -> CGSize {
        return .zero
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAt section: Int
    ) -> UIEdgeInsets {
        return .zero
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        let count = gameBoardValues.count

        guard count > 0 else {
            return .zero
        }

        let gameBoardWidth: CGFloat = gameBoard.bounds.size.width

        let gameCellSize: CGFloat = gameBoardWidth / CGFloat(count)

        return CGSize(width: gameCellSize, height: gameCellSize)
    }

}
