//
//  GameCollectionViewCell.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 28.05.2021.
//

import UIKit

class GameCollectionViewCell: UICollectionViewCell {

    // MARK: - Outlets

    @IBOutlet private weak var cellLabel: UILabel!

    // MARK: - Public properties

    // MARK: - Private properties

    // MARK: - Lifecycle

    // MARK: - Public methods

    func updateUI(with value: String, userMove: Bool) {
        let valueColor: UIColor
        let valueText: String

        switch value {
            case "x":
                valueColor = UIColor(hex: 0x7856D9)
                valueText = "x"
            case "o":
                valueColor = UIColor(hex: 0xFF0000)
                valueText = "o"
            default:
                valueColor = UIColor(hex: 0xECECEC)
                valueText = userMove ? "x" : "o"
        }

        let textSize: CGFloat = self.bounds.width * 1.2

        cellLabel.font = .ownHand(size: textSize)
        cellLabel.text = valueText
        cellLabel.textColor = valueColor
    }

    // MARK: - Private methods

    // MARK: - Outlet actions
    
}
