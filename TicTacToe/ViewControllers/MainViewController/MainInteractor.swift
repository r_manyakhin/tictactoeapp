//
//  MainInteractor.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 26.05.2021.
//

import Foundation

protocol MainInteractorProtocol {

    func getGame(completion: (Game) -> Void)

}

class MainInteractor: MainInteractorProtocol {

    // MARK: - Public properties

    // MARK: - Private properties

    private weak var presenter: MainPresenterProtocol?

    // MARK: - Lifecycle

    init(_ presenter: MainPresenterProtocol) {
        self.presenter = presenter
    }

    // MARK: - Public methods

    func getGame(completion: (Game) -> Void) {
        let game = Game(boardValues: Array(repeating: Array(repeating: "", count: gameLevel), count: gameLevel))

        completion(game)
    }

    // MARK: - Private methods

}
