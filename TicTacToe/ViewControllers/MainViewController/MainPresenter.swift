//
//  MainPresenter.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 26.05.2021.
//

import Foundation

protocol MainPresenterProtocol: class {

    func startButtonClicked()

}

class MainPresenter: MainPresenterProtocol {

    // MARK: - Public properties

    var interator: MainInteractorProtocol?

    // MARK: - Private properties

    private weak var view: MainViewProtocol?
    private var wireframe: MainWireframeProtocol?

    // MARK: - Lifecycle

    init(_ view: MainViewProtocol, wireframe: MainWireframeProtocol?) {
        self.view = view
        self.wireframe = wireframe
    }

    // MARK: - Public methods

    func startButtonClicked() {
        interator?.getGame(completion: { [weak self] game in
            guard let self = self else { return }

            self.wireframe?.showGame(game)
        })
    }

    // MARK: - Private methods

}
