//
//  MainViewController.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 26.05.2021.
//

import UIKit

protocol MainViewProtocol: class {
}

class MainViewController: UIViewController {

    // MARK: - Outlets

    @IBOutlet private weak var startButton: UIButton!

    // MARK: - Public properties

    var presenter: MainPresenterProtocol?

    // MARK: - Private properties

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        startButton.setTitle("Start game".localize, for: .normal)
        startButton.setTitleColor(.purple, for: .normal)
        startButton.addTarget(self, action: #selector(startButtonClicked(_:)), for: .touchUpInside)
    }

    // MARK: - Public methods

    // MARK: - Private methods

    // MARK: - Outlet actions

    @IBAction private func startButtonClicked(_ sender: UIButton) {
        presenter?.startButtonClicked()
    }

}

// MARK: - MainViewProtocol

extension MainViewController: MainViewProtocol {
}
