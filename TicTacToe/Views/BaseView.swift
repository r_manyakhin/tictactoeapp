//
//  BaseView.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 26.05.2021.
//

import UIKit

class BaseView: UIView, NibInitializable {

    // MARK: - Public properties

    var view: UIView! {
        didSet {
            configureUI()
        }
    }

    // MARK: - Private properties

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)

        nibSetup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        nibSetup()
    }

    // MARK: - Public methods

    override func awakeFromNib() {
        super.awakeFromNib()

        configureUI()
    }

    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()

        configureUI()
    }

    func configureUI() {
    }

    // MARK: - Private methods

}
