//
//  AlertWireframe.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 28.05.2021.
//

import UIKit

protocol AlertWireframeProtocol {

    func hideAlert(animated: Bool, completion: (() -> Void)?)

}

extension AlertWireframeProtocol {

    func hideAlert(animated: Bool = true, completion: (() -> Void)? = nil) {
        hideAlert(animated: animated, completion: completion)
    }

}

class AlertWireframe: AlertWireframeProtocol {

    // MARK: - Public properties

    // MARK: - Private properties

    private weak var viewController: UIViewController?

    // MARK: - Lifecycle

    init(with viewController: UIViewController? = nil) {
        self.viewController = viewController
    }

    // MARK: - Public methods

    func hideAlert(animated: Bool, completion: (() -> Void)?) {
        viewController?.dismiss(animated: animated, completion: completion)
    }

    // MARK: - Private methods

}
