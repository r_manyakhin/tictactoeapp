//
//  MainWireframe.swift
//  TicTacToe
//
//  Created by Roman Manyakhin on 26.05.2021.
//

import UIKit

protocol MainWireframeProtocol {

    func showMain()

    func showGame(_ game: Game)

    func popController()

    func showAlert(_ alert: Alert)

}

class MainWireframe: MainWireframeProtocol {

    // MARK: - Public properties

    // MARK: - Private properties

    private weak var viewController: UIViewController?

    private var window: UIWindow?

    // MARK: - Lifecycle

    init(with window: UIWindow? = nil) {
        self.window = window
    }

    // MARK: - Public methods

    func showMain() {
        let mainViewController = MainViewController.controller(from: .main)
        let mainPresenter = MainPresenter(mainViewController, wireframe: self)
        let mainInteractor = MainInteractor(mainPresenter)

        mainPresenter.interator = mainInteractor
        mainViewController.presenter = mainPresenter

        let navigationController = UINavigationController(rootViewController: mainViewController)
        navigationController.isNavigationBarHidden = true

        viewController = navigationController

        window?.rootViewController = navigationController

        window?.makeKeyAndVisible()
    }

    func showGame(_ game: Game) {
        let gameViewController = GameViewController.controller(from: .game)
        let gamePresenter = GamePresenter(view: gameViewController, wireframe: self)
        let gameInteractor = GameInteractor(presenter: gamePresenter, service: GameService(with: game))

        gamePresenter.interator = gameInteractor
        gameViewController.presenter = gamePresenter

        (viewController as? UINavigationController)?.pushViewController(gameViewController, animated: true)
    }

    func popController() {
        (viewController as? UINavigationController)?.popViewController(animated: true)
    }

    func showAlert(_ alert: Alert) {
        let alertViewController = AlertViewController.controller(from: .alert)
        alertViewController.modalPresentationStyle = .overCurrentContext
        alertViewController.modalTransitionStyle = .crossDissolve

        let alertWireframe = AlertWireframe(with: alertViewController)
        let alertPresenter = AlertPresenter(with: alert, view: alertViewController, wireframe: alertWireframe)

        alertViewController.presenter = alertPresenter

        (viewController as? UINavigationController)?.present(alertViewController, animated: true)
    }

    // MARK: - Private methods

}
